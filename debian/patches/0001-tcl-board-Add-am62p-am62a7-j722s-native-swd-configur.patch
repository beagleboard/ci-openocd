From af41c35511d634e89c9f9d10fef94061c7cf48e8 Mon Sep 17 00:00:00 2001
From: Nishanth Menon <nm@ti.com>
Date: Thu, 16 May 2024 15:49:37 -0500
Subject: [PATCH] tcl/board: Add am62p/am62a7/j722s native swd configuration

Direct memory driver swd native configuration for am62a7, am62p and
J722S SoCs. All three share common memory map for the debug address
map, so there is a strong reuse. However, introduce board file
specific to the board to allow users to directly get started.

Change-Id: I5609925a2e9918fd4c91d9fd40fbee98de27fdbc
Signed-off-by: Nishanth Menon <nm@ti.com>
---
 tcl/board/ti_am62a7_swd_native.cfg | 22 ++++++++++++++++++++++
 tcl/board/ti_am62p_swd_native.cfg  | 22 ++++++++++++++++++++++
 tcl/board/ti_j722s_swd_native.cfg  | 23 +++++++++++++++++++++++
 tcl/target/ti_k3.cfg               | 10 ++++++++++
 4 files changed, 77 insertions(+)
 create mode 100644 tcl/board/ti_am62a7_swd_native.cfg
 create mode 100644 tcl/board/ti_am62p_swd_native.cfg
 create mode 100644 tcl/board/ti_j722s_swd_native.cfg

diff --git a/tcl/board/ti_am62a7_swd_native.cfg b/tcl/board/ti_am62a7_swd_native.cfg
new file mode 100644
index 000000000..99fc0b0b3
--- /dev/null
+++ b/tcl/board/ti_am62a7_swd_native.cfg
@@ -0,0 +1,22 @@
+# SPDX-License-Identifier: GPL-2.0-or-later
+# Copyright (C) 2024 Texas Instruments Incorporated - http://www.ti.com/
+#
+# Texas Instruments AM62A7
+# Link: https://www.ti.com/product/AM62A7
+#
+# This configuration file is used as a self hosted debug configuration that
+# works on every AM62A7 platform based on firewall configuration permitted
+# in the system.
+#
+# In this system openOCD runs on one of the CPUs inside AM62A7 and provides
+# network ports that can then be used to debug the microcontrollers on the
+# SoC - either self hosted IDE OR remotely.
+
+# We are using dmem, which uses dapdirect_swd transport
+adapter driver dmem
+
+if { ![info exists SOC] } {
+	set SOC am62a7
+}
+
+source [find target/ti_k3.cfg]
diff --git a/tcl/board/ti_am62p_swd_native.cfg b/tcl/board/ti_am62p_swd_native.cfg
new file mode 100644
index 000000000..fa549f358
--- /dev/null
+++ b/tcl/board/ti_am62p_swd_native.cfg
@@ -0,0 +1,22 @@
+# SPDX-License-Identifier: GPL-2.0-or-later
+# Copyright (C) 2024 Texas Instruments Incorporated - http://www.ti.com/
+#
+# Texas Instruments am62p
+# Link: https://www.ti.com/product/AM62P
+#
+# This configuration file is used as a self hosted debug configuration that
+# works on every AM62P platform based on firewall configuration permitted
+# in the system.
+#
+# In this system openOCD runs on one of the CPUs inside AM62P and provides
+# network ports that can then be used to debug the microcontrollers on the
+# SoC - either self hosted IDE OR remotely.
+
+# We are using dmem, which uses dapdirect_swd transport
+adapter driver dmem
+
+if { ![info exists SOC] } {
+	set SOC am62p
+}
+
+source [find target/ti_k3.cfg]
diff --git a/tcl/board/ti_j722s_swd_native.cfg b/tcl/board/ti_j722s_swd_native.cfg
new file mode 100644
index 000000000..bbe0d508c
--- /dev/null
+++ b/tcl/board/ti_j722s_swd_native.cfg
@@ -0,0 +1,23 @@
+# SPDX-License-Identifier: GPL-2.0-or-later
+# Copyright (C) 2024 Texas Instruments Incorporated - http://www.ti.com/
+#
+# Texas Instruments J722S/AM67/TDA4VEN
+# Link: https://www.ti.com/product/AM67
+# Link: https://www.ti.com/product/TDA4VEN-Q1
+#
+# This configuration file is used as a self hosted debug configuration that
+# works on every J722S platform based on firewall configuration permitted
+# in the system.
+#
+# In this system openOCD runs on one of the CPUs inside J722S and provides
+# network ports that can then be used to debug the microcontrollers on the
+# SoC - either self hosted IDE OR remotely.
+
+# We are using dmem, which uses dapdirect_swd transport
+adapter driver dmem
+
+if { ![info exists SOC] } {
+	set SOC j722s
+}
+
+source [find target/ti_k3.cfg]
diff --git a/tcl/target/ti_k3.cfg b/tcl/target/ti_k3.cfg
index ebea82179..2ae0f75b8 100644
--- a/tcl/target/ti_k3.cfg
+++ b/tcl/target/ti_k3.cfg
@@ -209,6 +209,16 @@ switch $_soc {
 		# Sysctrl power-ap unlock offsets
 		set _sysctrl_ap_unlock_offsets {0xf0 0x78}
 
+		# Setup DMEM access descriptions
+		# DAPBUS (Debugger) description
+		set _dmem_base_address 0x740002000
+		set _dmem_ap_address_offset 0x100
+		set _dmem_max_aps 10
+		# Emulated AP description
+		set _dmem_emu_base_address 0x760000000
+		set _dmem_emu_base_address_map_to 0x1d500000
+		set _dmem_emu_ap_list 1
+
 		# Overrides for am62p
 		if { "$_soc" == "am62p" } {
 			set _K3_DAP_TAPID 0x0bb9d02f
-- 
2.39.2

